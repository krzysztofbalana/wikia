Dzień dobry!

Postaram się opisać tutaj podejście jakie wybrałem, czego nie zdążyłem oraz z czego zrezygnowałem ;)

No więc podejście: Pomijając Clean Architecture, które lubię i szanuję, zdecydowałem się zaaranżowanie projektu wg package by feature. Jest to mój pierwszy raz. Do tej pory raczej dzielilem projekty per warstwa, najczęściej z pomocą odpowiednich modułów (presentation/domain/data). Poszczególne elementy łączyłem abstrakcja. Czyli standardowo. 

Na front wybrałem (z ciekawości w sumie) MVVM (zamiast MVP) razem z livedata, również po raz pierwszy :) W esky jesteśmy mocno związani z presenterami, a właściwie własną wariacją na temat (nazywamy to modułami), natomiast po godzinach uczę się innych rzeczy. W związku z powyżym testy na ui ucierpiały. Miałem trochę zawrót głowy, żeby to poprawnie przetestować i ostatecznie (z powodu uciekającego czasu) odpuściłem.

Nie zdążyłem również napisać repozytorium do przechowywania danych/preferencji szukania. Zapewne użyłbym Realm, nie mam doświadczenia z sql (w androidzie) ani z Room. Usypiam na pisaniu query do sql, nic mnie bardziej nie nudzi ;) Ponadto, przeoczyłem fragment mówiący o tym, żeby nad tabami znalazł się nagłówek. Z rozpędu wrzuciłem lekko zmodyufikowane zdjęcie :) Mam nadzieję, że to nie problem.

Z czego zrezygnowałem w tym przypadku (chyba nie usunąłem zależności), to RxJava. Wiem, że mocno korzystacie (zdekompilowałem sobie waszą apkę - o tyle o ile oczywiście). Ogólnie lubię i w miarę regularnie korzystam, ale tym razem służyłaby wyłącznie do komunikacji restowej. Stwierdziłem, że można spróbować z coroutines, skoro tak mocno je promują ;)

Generalnie, apce brakuje jeszcze (m.in.) mocnego refaktoru nazewnictwa. Nie poczułem od razu przyjaźni do PBF*, miałem trochę problem ze zbyt dużą ilośćią klas w jednym pakiecie. Trochę z tym walczyłem ;) Jak widać po finalnym podziale, trudno mi odpuścić stare przyzwyczajenia.

No! To tyle ode mnie. To do zobaczenia!

* Package By Feature

