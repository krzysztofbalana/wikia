package com.wikia.fandom.fandomnews.features.topwikis.domain

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class WikiaPhotosEntityMapeprTest {

    @Test
    fun `should convert entity to model properly`() {
        val dummyData = mutableListOf(
                MostPopularWikiaPhotoEntity("1"),
                MostPopularWikiaPhotoEntity("2"),
                MostPopularWikiaPhotoEntity("3"))

        val result = WikiaPhotosEntityMapepr.convert(dummyData)

        assertThat(result).hasSize(3)
        assertThat(result).extracting("imageUrl").contains("1", "2", "3")

    }
}