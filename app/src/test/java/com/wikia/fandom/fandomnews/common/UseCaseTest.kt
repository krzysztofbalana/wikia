package com.wikia.fandom.fandomnews.common

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import kotlinx.coroutines.experimental.Unconfined
import kotlinx.coroutines.experimental.runBlocking
import org.junit.Before

import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import kotlin.coroutines.experimental.CoroutineContext


@RunWith(MockitoJUnitRunner::class)
class UseCaseTest {

    lateinit var systemUnderTest: TestUseCase

    @Before
    fun setUp() {
        systemUnderTest = TestUseCase(Unconfined, Unconfined)
    }

    @Test
    fun `should return type of Result`() {
        val callback: (String) -> Unit = mock()

        runBlocking { systemUnderTest.execute(callback) }

        verify(callback).invoke(any<String>())

    }

    class TestUseCase(uiContext: CoroutineContext, ioContext: CoroutineContext): UseCase<String>(uiContext, ioContext) {
        override suspend fun buildUseCase(): String {
            return ""
        }
    }
}