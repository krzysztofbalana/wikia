package com.wikia.fandom.fandomnews.features.topwikis.domain

import com.wikia.fandom.fandomnews.features.topwikis.domain.MostPopularWikiaEntity
import com.wikia.fandom.fandomnews.features.topwikis.domain.WikiaEntityMapper
import org.junit.Test

import org.assertj.core.api.Assertions.*


class WikiaEntityMapperTest {

    @Test
    fun `should convert entity to model properly`() {
        val dummyData = mutableListOf(MostPopularWikiaEntity(1, "1", 11), MostPopularWikiaEntity(2, "2", 22), MostPopularWikiaEntity(3, "3", 33))

        val result = WikiaEntityMapper.convert(dummyData)

        assertThat(result).hasSize(3).extracting("id").contains(1,2,3)
        assertThat(result).extracting("name").contains("1", "2", "3")
        assertThat(result).extracting("articlesCount").contains(11,22,33)
    }
}