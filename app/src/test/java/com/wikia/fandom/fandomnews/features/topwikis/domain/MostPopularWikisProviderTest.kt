package com.wikia.fandom.fandomnews.features.topwikis.domain

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import com.wikia.fandom.fandomnews.common.Result
import com.wikia.fandom.fandomnews.common.networking.RestApi
import com.wikia.fandom.fandomnews.features.topwikis.infrastructure.MostPopularWikisPhotosResponse
import com.wikia.fandom.fandomnews.features.topwikis.infrastructure.MostPopularWikisQuery
import com.wikia.fandom.fandomnews.features.topwikis.infrastructure.MostPopularWikisResponse
import com.wikia.fandom.fandomnews.features.topwikis.infrastructure.MostPopularWikisProvider
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit

@RunWith(MockitoJUnitRunner::class)
class MostPopularWikisProviderTest {

    @Mock
    lateinit var restApi: RestApi
    @Mock
    lateinit var retrofit: Retrofit
    lateinit var classUnderTest: MostPopularWikisProvider.Implementation

    @Before
    fun setUp() {
        classUnderTest = MostPopularWikisProvider.Implementation(restApi)
    }

    @Test
    fun `should be able to return success object after list successful fetch`() {
        prepareListSuccessfulResponse()

        val result = classUnderTest.loadTopWikis(100)

        assertThat(result).isExactlyInstanceOf(Result.Success::class.java)
    }

    @Test
    fun `should be able return succes object after photos successful fetch`() {
        preparePhotosSuccessfulResponse()

        val result = classUnderTest.loadTopWikisImages(100)

        assertThat(result).isExactlyInstanceOf(Result.Success::class.java)
    }

    @Test
    fun `should return error object after fetch doesnt finish with success`() {
        simulateDataCannotBeFetched()

        val result = classUnderTest.loadTopWikis(100)

        assertThat(result).isExactlyInstanceOf(Result.Failure::class.java)
    }

    private fun simulateDataCannotBeFetched() {
        prepareRetrofitMock()

        val mockedQuery: MostPopularWikisQuery = mock()
        val mockedCall: Call<MostPopularWikisResponse> = mock()
        val mockedResponse: Response<MostPopularWikisResponse> = mock()

        whenever(retrofit.create(eq(MostPopularWikisQuery::class.java))).thenReturn(mockedQuery)
        whenever(mockedQuery.getList(any())).thenReturn(mockedCall)
        whenever(mockedCall.execute()).thenReturn(mockedResponse)
        whenever(mockedResponse.isSuccessful).thenReturn(false)
    }

    private fun prepareListSuccessfulResponse() {
        prepareRetrofitMock()

        val mockedQuery: MostPopularWikisQuery = mock()
        val mockedCall: Call<MostPopularWikisResponse> = mock()
        val mockedResponse: Response<MostPopularWikisResponse> = mock()

        whenever(retrofit.create(eq(MostPopularWikisQuery::class.java))).thenReturn(mockedQuery)
        whenever(mockedQuery.getList(any())).thenReturn(mockedCall)
        whenever(mockedCall.execute()).thenReturn(mockedResponse)
        whenever(mockedResponse.isSuccessful).thenReturn(true)
        whenever(mockedResponse.body()).thenReturn(MostPopularWikisResponse(emptyList()))
    }

    private fun preparePhotosSuccessfulResponse() {
        prepareRetrofitMock()

        val mockedQuery: MostPopularWikisQuery = mock()
        val mockedCall: Call<MostPopularWikisPhotosResponse> = mock()
        val mockedResponse: Response<MostPopularWikisPhotosResponse> = mock()

        whenever(retrofit.create(eq(MostPopularWikisQuery::class.java))).thenReturn(mockedQuery)
        whenever(mockedQuery.getPhotos(any())).thenReturn(mockedCall)
        whenever(mockedCall.execute()).thenReturn(mockedResponse)
        whenever(mockedResponse.isSuccessful).thenReturn(true)
        whenever(mockedResponse.body()).thenReturn(MostPopularWikisPhotosResponse(emptyList()))
    }

    private fun prepareRetrofitMock() {
        whenever(restApi.using(eq(MostPopularWikisProvider.BASE_URL))).thenReturn(restApi)
        whenever(restApi.load()).thenReturn(retrofit)
    }
}