package com.wikia.fandom.fandomnews.features.topwikis.infrastructure

import org.junit.Test
import org.assertj.core.api.Assertions.*


class MostPopularListDtoMapperTest {

    @Test
    fun `should convert to domain model properly`() {
        val dummyData = mutableListOf(MostPopularWikiaDTO(1, "1", MostPopularWikiaDTO.Stats(100)), MostPopularWikiaDTO(2, "2", MostPopularWikiaDTO.Stats(200)))

        val result = ListDtoMapper.convert(dummyData)

        assertThat(result).hasSize(2).extracting("id").contains(1,2)
        assertThat(result).extracting("name").contains("1", "2")
        assertThat(result).extracting("articlesCount").contains(100,200)
    }
}