package com.wikia.fandom.fandomnews.features.topwikis.domain

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.wikia.fandom.fandomnews.common.Result
import com.wikia.fandom.fandomnews.common.utils.ErrorType
import com.wikia.fandom.fandomnews.features.topwikis.infrastructure.MostPopularWikisProvider
import com.wikia.fandom.fandomnews.features.topwikis.presentation.grid.MostPopularWikisPhotos
import kotlinx.coroutines.experimental.Unconfined
import kotlinx.coroutines.experimental.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetWikisImagesUseCaseTest {

    @Mock
    lateinit var mostPopularWikisProvider: MostPopularWikisProvider

    lateinit var classUnderTest: GetWikisImagesUseCase

    @Before
    fun setUp() {
        classUnderTest = GetWikisImagesUseCase(mostPopularWikisProvider, Unconfined, Unconfined)

    }

    @Test
    fun `should return data after successful fetch`() {
        val mockedCallback: (MostPopularWikisPhotos) -> Unit = mock()
        `when`(mostPopularWikisProvider.loadTopWikisImages(any())).thenReturn(Result.Success(mutableListOf(MostPopularWikiaPhotoEntity("image_url"))))

        runBlocking { classUnderTest.execute(mockedCallback) }

        verify(mostPopularWikisProvider).loadTopWikisImages(any())
        verify(mockedCallback).invoke(any<MostPopularWikisPhotos.Data>())
    }

    @Test
    fun `should return failure after unsuccessful fetch`() {
        val mockedCallback: (MostPopularWikisPhotos) -> Unit = mock()
        `when`(mostPopularWikisProvider.loadTopWikisImages(any())).thenReturn(Result.Failure(ErrorType.UNDEFINED))

        runBlocking { classUnderTest.execute(mockedCallback) }

        verify(mockedCallback).invoke(any<MostPopularWikisPhotos.Error>())

    }
}