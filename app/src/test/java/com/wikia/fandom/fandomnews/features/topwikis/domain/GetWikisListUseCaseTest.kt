package com.wikia.fandom.fandomnews.features.topwikis.domain

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.wikia.fandom.fandomnews.common.Result
import com.wikia.fandom.fandomnews.common.utils.ErrorType
import com.wikia.fandom.fandomnews.features.topwikis.infrastructure.MostPopularWikisProvider
import com.wikia.fandom.fandomnews.features.topwikis.presentation.list.MostPopularWikis
import kotlinx.coroutines.experimental.Unconfined
import kotlinx.coroutines.experimental.runBlocking
import org.junit.Before

import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetWikisListUseCaseTest {

    @Mock
    lateinit var mostPopularWikisProvider: MostPopularWikisProvider

    lateinit var classUnderTest: GetWikisListUseCase

    @Before
    fun setUp() {
        classUnderTest = GetWikisListUseCase(mostPopularWikisProvider, Unconfined, Unconfined)
    }

    @Test
    fun `should return data after successful fetch`() {
        val mockedCallback: (MostPopularWikis) -> Unit = mock()
        `when`(mostPopularWikisProvider.loadTopWikis(any())).thenReturn(Result.Success(mutableListOf(MostPopularWikiaEntity(11, "", 11))))

        runBlocking { classUnderTest.execute(mockedCallback) }

        verify(mostPopularWikisProvider).loadTopWikis(any())

        verify(mockedCallback).invoke(any<MostPopularWikis.Data>())
    }

    @Test
    fun `should return failure after unsuccessful fetch`() {
        val mockedCallback: (MostPopularWikis) -> Unit = mock()
        `when`(mostPopularWikisProvider.loadTopWikis(any())).thenReturn(Result.Failure(ErrorType.UNDEFINED))

        runBlocking { classUnderTest.execute(mockedCallback) }

        verify(mockedCallback).invoke(any<MostPopularWikis.Error>())
    }
}