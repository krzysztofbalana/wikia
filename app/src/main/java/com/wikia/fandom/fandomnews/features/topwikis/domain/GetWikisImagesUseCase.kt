package com.wikia.fandom.fandomnews.features.topwikis.domain

import com.wikia.fandom.fandomnews.common.Result
import com.wikia.fandom.fandomnews.common.UseCase
import com.wikia.fandom.fandomnews.common.utils.ErrorType
import com.wikia.fandom.fandomnews.features.topwikis.infrastructure.MostPopularWikisProvider
import com.wikia.fandom.fandomnews.features.topwikis.presentation.grid.MostPopularWikisPhotos
import javax.inject.Inject
import kotlin.coroutines.experimental.CoroutineContext

class GetWikisImagesUseCase @Inject constructor(private val mostPopularWikisProvider: MostPopularWikisProvider, uiContext: CoroutineContext, ioContext: CoroutineContext): UseCase<MostPopularWikisPhotos>(uiContext, ioContext) {

    companion object {
        var LIMIT = 30
    }

    override suspend fun buildUseCase(): MostPopularWikisPhotos {
        val result: Result<List<MostPopularWikiaPhotoEntity>> = mostPopularWikisProvider.loadTopWikisImages(LIMIT)
        return when (result) {
            is Result.Success -> {
                MostPopularWikisPhotos.Data(WikiaPhotosEntityMapepr.convert(result.data))
            }
            is Result.Failure -> {
                when (result.typeOfError) {
                    ErrorType.BAD_REQUEST -> MostPopularWikisPhotos.Error("error message and type")
                    else -> MostPopularWikisPhotos.Error("error message and type")
                }
            }
        }
    }
}