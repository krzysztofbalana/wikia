package com.wikia.fandom.fandomnews.features.topwikis

import com.wikia.fandom.fandomnews.features.topwikis.domain.GetWikisImagesUseCase
import com.wikia.fandom.fandomnews.features.topwikis.domain.GetWikisListUseCase
import com.wikia.fandom.fandomnews.features.topwikis.infrastructure.MostPopularWikisProvider
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.android.UI
import javax.inject.Singleton

@Module
class MostPopularWikisModule {

    /**
     * normally it should get own scope along with dedicated feature subcompononent
     */
    @Singleton
    @Provides
    fun provideWikisProvider(dataSource: MostPopularWikisProvider.Implementation): MostPopularWikisProvider = dataSource

    @Singleton
    @Provides
    fun provideGetWikisListUseCase(mostPopularWikisProvider: MostPopularWikisProvider.Implementation) = GetWikisListUseCase(mostPopularWikisProvider, UI, CommonPool)

    @Singleton
    @Provides
    fun provivdeGetWikisImagesUseCase(mostPopularWikisProvider: MostPopularWikisProvider.Implementation) = GetWikisImagesUseCase(mostPopularWikisProvider, UI, CommonPool)
}