package com.wikia.fandom.fandomnews.features.topwikis

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import com.wikia.fandom.fandomnews.common.utils.ViewPagerAdapter
import com.wikia.fandom.fandomnews.features.topwikis.presentation.grid.MostPopularWikisGridFragment
import com.wikia.fandom.fandomnews.features.topwikis.presentation.list.MostPopularWikisListFragment

class MostPopularWikisPageAdapter(fragmentManager: FragmentManager) :ViewPagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> MostPopularWikisListFragment()
            1 -> MostPopularWikisGridFragment()
            else -> {
                MostPopularWikisListFragment()
            }
        }
    }
}