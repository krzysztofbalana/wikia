package com.wikia.fandom.fandomnews.common.networking

import retrofit2.Retrofit

interface RestApi {

    open fun load(): Retrofit

    open fun using(baseUrl: String): RestApi
}