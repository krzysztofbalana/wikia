package com.wikia.fandom.fandomnews.features.topwikis.infrastructure

import com.google.gson.annotations.SerializedName

class MostPopularWikisResponse(

        @SerializedName("items")
        val listOfNews: List<MostPopularWikiaDTO>

)

class MostPopularWikisPhotosResponse(

        @SerializedName("items")
        val photos: List<MostPopularWikiaPhotosDTO>
)
