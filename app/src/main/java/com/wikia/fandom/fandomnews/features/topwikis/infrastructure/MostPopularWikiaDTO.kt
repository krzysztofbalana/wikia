package com.wikia.fandom.fandomnews.features.topwikis.infrastructure

import com.google.gson.annotations.SerializedName

class MostPopularWikiaDTO(
        @SerializedName("id") val id: Int,
        @SerializedName("name") val name: String,
        @SerializedName("stats") val stats: Stats
) {
    class Stats(@SerializedName("articles") val articlesCount: Int)
}
