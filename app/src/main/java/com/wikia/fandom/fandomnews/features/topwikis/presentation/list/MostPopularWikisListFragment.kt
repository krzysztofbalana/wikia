package com.wikia.fandom.fandomnews.features.topwikis.presentation.list


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import com.wikia.fandom.fandomnews.R
import com.wikia.fandom.fandomnews.common.base.BaseFragment
import javax.inject.Inject


class MostPopularWikisListFragment : BaseFragment() {

    @BindView(R.id.list_container)
    lateinit var listContainer: RecyclerView
    lateinit var viewModel: MostPopularWikisListViewModel
    @Inject lateinit var adapter: MostPopularWikisListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent.inject(this)

        viewModel = ViewModelProviders.of(this, viewModelFactory)[MostPopularWikisListViewModel::class.java]
        viewModel.topWikis.observe(this, Observer {
            if (it != null) {
                displayResults(it)
            }
        })

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_top_wikis_list, container, false)
        ButterKnife.bind(this, view)

        viewModel.loadWikis()
        listContainer.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        listContainer.adapter = adapter

        return view
    }

    private fun displayResults(results: List<MostPopularWikis.Wikia>) {
        adapter.upateResults(results)
    }

}
