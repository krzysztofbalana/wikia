package com.wikia.fandom.fandomnews.common

import android.app.Application
import com.wikia.fandom.fandomnews.common.di.AppComponent
import com.wikia.fandom.fandomnews.common.di.AppModule
import com.wikia.fandom.fandomnews.common.di.DaggerAppComponent

class App : Application() {

    val appComponent: AppComponent by lazy(mode = LazyThreadSafetyMode.NONE) {
        DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .build()
    }

    override fun onCreate() {
        super.onCreate()
        appComponent.inject(this)
    }
}