package com.wikia.fandom.fandomnews.features.topwikis.presentation.grid


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife

import com.wikia.fandom.fandomnews.R
import com.wikia.fandom.fandomnews.common.base.BaseFragment
import javax.inject.Inject


class MostPopularWikisGridFragment : BaseFragment() {

    @BindView(R.id.grid_container)
    lateinit var photosContainer: RecyclerView

    lateinit var viewModel: MostPopularWikisGridViewModel
    @Inject lateinit var adapter: MostPopularWikisGridAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent.inject(this)

        viewModel = ViewModelProviders.of(this, viewModelFactory)[MostPopularWikisGridViewModel::class.java]

        viewModel.topPhotos.observe(this, Observer {
            if (it != null) {
                displayResults(it)
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_top_wikis_grid, container, false)
        ButterKnife.bind(this, view)

        viewModel.loadPhotos()

        photosContainer.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        photosContainer.adapter = adapter
        return view
    }

    private fun displayResults(list: List<MostPopularWikisPhotos.WikiaPhoto>) {
        adapter.upateResults(list)
    }

}
