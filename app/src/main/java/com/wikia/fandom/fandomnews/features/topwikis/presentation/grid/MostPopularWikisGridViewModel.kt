package com.wikia.fandom.fandomnews.features.topwikis.presentation.grid

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.wikia.fandom.fandomnews.features.topwikis.domain.GetWikisImagesUseCase
import javax.inject.Inject

class MostPopularWikisGridViewModel@Inject constructor(private val getTopWikisList: GetWikisImagesUseCase) : ViewModel() {

    var topPhotos: MutableLiveData<List<MostPopularWikisPhotos.WikiaPhoto>> = MutableLiveData()

    fun loadPhotos() {
        getTopWikisList.execute {
            when(it) {
                is MostPopularWikisPhotos.Data -> handleList(it.topWikisPhotos)
                is MostPopularWikisPhotos.Error -> handleError()
            }
        }
    }

    private fun handleError() {
        //handle error here
    }

    private fun handleList(topPhotos: List<MostPopularWikisPhotos.WikiaPhoto>) {
        this.topPhotos.value = topPhotos
    }
}