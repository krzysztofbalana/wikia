package com.wikia.fandom.fandomnews.features.topwikis.infrastructure

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface MostPopularWikisQuery {

    @GET("List?expand=1&batch=1")
    fun getList(@Query("limit") limit: Int): Call<MostPopularWikisResponse>

    @GET("List?expand=1&batch=1")
    fun getPhotos(@Query("limit") limit: Int): Call<MostPopularWikisPhotosResponse>


}