package com.wikia.fandom.fandomnews.common.utils

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

abstract class ViewPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    val fragmentsList = mutableListOf<Fragment>()
    val fragmentsTitles = mutableListOf<String>()

    fun addFragment(fragment: Fragment, title: String): ViewPagerAdapter {
        fragmentsList.add(fragment)
        fragmentsTitles.add(title)
        return this
    }

    abstract override fun getItem(position: Int): Fragment

    override fun getCount(): Int = fragmentsList.size

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> fragmentsTitles[position]
            1 -> fragmentsTitles[position]
            else -> " "
        }
    }

}