package com.wikia.fandom.fandomnews.features.topwikis.infrastructure

import com.wikia.fandom.fandomnews.common.utils.Mapper
import com.wikia.fandom.fandomnews.features.topwikis.domain.MostPopularWikiaEntity
import com.wikia.fandom.fandomnews.features.topwikis.domain.MostPopularWikiaPhotoEntity

object ListDtoMapper: Mapper<List<MostPopularWikiaDTO>, List<MostPopularWikiaEntity>> {
    override fun convert(data: List<MostPopularWikiaDTO>): List<MostPopularWikiaEntity> {
        return data.map { MostPopularWikiaEntity(it.id, it.name, it.stats.articlesCount) }
    }
}

object PhotosDtoMapper: Mapper<List<MostPopularWikiaPhotosDTO>, List<MostPopularWikiaPhotoEntity>> {

    override fun convert(data: List<MostPopularWikiaPhotosDTO>): List<MostPopularWikiaPhotoEntity> {
        return data.map { MostPopularWikiaPhotoEntity(it.image) }
    }
}
