package com.wikia.fandom.fandomnews.features.topwikis.presentation.list

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.wikia.fandom.fandomnews.R
import javax.inject.Inject

class MostPopularWikisListAdapter @Inject constructor(val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var results: List<MostPopularWikis.Wikia> = emptyList()

    fun upateResults(results: List<MostPopularWikis.Wikia>) {
        this.results = results
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val view = LayoutInflater.from(context).inflate(R.layout.list_wikis_item, parent, false)
        return ViewHolder(view)

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder) {
            is ViewHolder -> holder.bind(results[position])
        }
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        @BindView(R.id.title)
        lateinit var title: TextView

        @BindView(R.id.articlesCount)
        lateinit var articlesCount: TextView

        init {
            ButterKnife.bind(this, view)
        }

        fun bind(data: MostPopularWikis.Wikia) {

            title.text = data.name
            articlesCount.text = "${data.articlesCount}"

        }

    }

    override fun getItemCount(): Int {
        return results.size
    }
}
