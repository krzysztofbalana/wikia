package com.wikia.fandom.fandomnews.features.topwikis.presentation.grid

sealed class MostPopularWikisPhotos {
    class Data(val topWikisPhotos: List<WikiaPhoto>): MostPopularWikisPhotos()
    class Error(val errorMessage: String): MostPopularWikisPhotos()
    class WikiaPhoto(val imageUrl: String)
}