package com.wikia.fandom.fandomnews.features.topwikis.domain

class MostPopularWikiaEntity(val id: Int, val name: String, val articlesCount: Int)
