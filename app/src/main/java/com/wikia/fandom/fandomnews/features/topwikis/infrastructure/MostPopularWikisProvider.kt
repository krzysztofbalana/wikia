package com.wikia.fandom.fandomnews.features.topwikis.infrastructure

import com.wikia.fandom.fandomnews.common.Result
import com.wikia.fandom.fandomnews.common.networking.RestApi
import com.wikia.fandom.fandomnews.common.utils.ErrorType
import com.wikia.fandom.fandomnews.common.utils.HttpResponseParser
import com.wikia.fandom.fandomnews.features.topwikis.domain.MostPopularWikiaEntity
import com.wikia.fandom.fandomnews.features.topwikis.domain.MostPopularWikiaPhotoEntity
import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject


interface MostPopularWikisProvider {

    companion object {
        const val BASE_URL = "http://www.wikia.com/api/v1/Wikis/"
    }

    fun loadTopWikis(limit: Int): Result<List<MostPopularWikiaEntity>>
    fun loadTopWikisImages(limit: Int): Result<List<MostPopularWikiaPhotoEntity>>


    class Implementation @Inject constructor (private val restApi: RestApi) : MostPopularWikisProvider {

        override fun loadTopWikis(limit: Int): Result<List<MostPopularWikiaEntity>> {
            val retrofit = prepareClient()
            val queryMostPopular: MostPopularWikisQuery = retrofit.create(MostPopularWikisQuery::class.java)
            val call: Call<MostPopularWikisResponse> = queryMostPopular.getList(limit)

            val response: Response<MostPopularWikisResponse> = call.execute()

            return if (response.isSuccessful && response.body() != null) {
                val responseBody = response.body()

                when (responseBody) {
                    is MostPopularWikisResponse -> Result.Success(ListDtoMapper.convert(responseBody.listOfNews))
                    else -> {
                        Result.Failure(HttpResponseParser.parse(response.code()))
                    }
                }

            } else {
                Result.Failure(ErrorType.UNDEFINED)
            }
        }

        override fun loadTopWikisImages(limit: Int): Result<List<MostPopularWikiaPhotoEntity>> {
            val retrofit = prepareClient()
            val queryMostPopular: MostPopularWikisQuery = retrofit.create(MostPopularWikisQuery::class.java)
            val call: Call<MostPopularWikisPhotosResponse> = queryMostPopular.getPhotos(limit)

            val response: Response<MostPopularWikisPhotosResponse> = call.execute()

            return if (response.isSuccessful && response.body() != null) {
                val responseBody = response.body()

                when (responseBody) {
                    is MostPopularWikisPhotosResponse -> Result.Success(PhotosDtoMapper.convert(responseBody.photos))
                    else -> {
                        Result.Failure(HttpResponseParser.parse(response.code()))
                    }
                }

            } else {
                Result.Failure(ErrorType.UNDEFINED)
            }
        }

        private fun prepareClient() = restApi.using(BASE_URL).load()

    }
}
