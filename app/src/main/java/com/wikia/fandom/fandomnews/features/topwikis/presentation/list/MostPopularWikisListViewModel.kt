package com.wikia.fandom.fandomnews.features.topwikis.presentation.list

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.wikia.fandom.fandomnews.features.topwikis.domain.GetWikisListUseCase
import javax.inject.Inject

class MostPopularWikisListViewModel @Inject constructor(private val getTopWikisList: GetWikisListUseCase): ViewModel() {

    var topWikis: MutableLiveData<List<MostPopularWikis.Wikia>> = MutableLiveData()

    fun loadWikis() {
        getTopWikisList.execute {
            when(it) {
                is MostPopularWikis.Data -> handleList(it.topWikis)
                is MostPopularWikis.Error -> handleError()
            }
        }
    }

    private fun handleError() {
        //handle error here
    }

    private fun handleList(topWikis: List<MostPopularWikis.Wikia>) {
        this.topWikis.value = topWikis
    }
}