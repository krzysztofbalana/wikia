package com.wikia.fandom.fandomnews.features.topwikis.domain

import com.wikia.fandom.fandomnews.common.Result
import com.wikia.fandom.fandomnews.common.UseCase
import com.wikia.fandom.fandomnews.common.utils.ErrorType
import com.wikia.fandom.fandomnews.features.topwikis.infrastructure.MostPopularWikisProvider
import com.wikia.fandom.fandomnews.features.topwikis.presentation.list.MostPopularWikis
import javax.inject.Inject
import kotlin.coroutines.experimental.CoroutineContext

class GetWikisListUseCase @Inject constructor(private val mostPopularWikisProvider: MostPopularWikisProvider, uiContext: CoroutineContext, ioContext: CoroutineContext) : UseCase<MostPopularWikis>(uiContext, ioContext) {

    /**
     * Normally, business logic like wikis count should be kept outside particular UseCase
     * (for ex. config files, user settings, remote config files and so on)
     * although I've decided to keep it here for brevity.
     */
    companion object {
        var TOP_WIKIS_LIMIT = 30
    }


    override suspend fun buildUseCase(): MostPopularWikis {
        val result: Result<List<MostPopularWikiaEntity>> = mostPopularWikisProvider.loadTopWikis(TOP_WIKIS_LIMIT)
        return when (result) {
            is Result.Success -> {
                MostPopularWikis.Data(WikiaEntityMapper.convert(result.data))

            }
        /**
         * Because I've had limited time I've decided to omit error handling. However, in ideal world, this
         * would be the place (delegated to a supporting class) of designing an error type to a presentation layer,
         * so it could display proper error type and message to an end user!
         */

            is Result.Failure -> {
                when (result.typeOfError) {
                    ErrorType.BAD_REQUEST -> MostPopularWikis.Error("error message and type")
                    else -> MostPopularWikis.Error("error message and type")
                }
            }
        }
    }
}