package com.wikia.fandom.fandomnews.common.utils

interface Mapper<in T, out R> {

    fun convert(data: T): R
}