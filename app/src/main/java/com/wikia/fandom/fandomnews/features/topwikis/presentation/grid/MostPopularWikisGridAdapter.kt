package com.wikia.fandom.fandomnews.features.topwikis.presentation.grid

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import butterknife.BindView
import butterknife.ButterKnife
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.wikia.fandom.fandomnews.R
import javax.inject.Inject

class MostPopularWikisGridAdapter @Inject constructor(val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var results: List<MostPopularWikisPhotos.WikiaPhoto> = emptyList()

    fun upateResults(results: List<MostPopularWikisPhotos.WikiaPhoto>) {
        this.results = results
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.grid_wikis_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is MostPopularWikisGridAdapter.ViewHolder -> holder.bind(context, results[position])
        }
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        @BindView(R.id.image_holder)
        lateinit var imageHolder: ImageView

        init {
            ButterKnife.bind(this, view)
        }

        fun bind(context: Context, item: MostPopularWikisPhotos.WikiaPhoto) {
            Glide.with(context)
                    .load(item.imageUrl)
                    .apply(RequestOptions().centerCrop())
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(imageHolder)
        }
    }

    override fun getItemCount(): Int {
        return results.size
    }
}