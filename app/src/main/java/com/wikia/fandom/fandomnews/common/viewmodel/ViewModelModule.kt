package com.wikia.fandom.fandomnews.common.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.wikia.fandom.fandomnews.features.topwikis.presentation.list.MostPopularWikisListViewModel
import com.wikia.fandom.fandomnews.features.topwikis.presentation.ViewModelFactory
import com.wikia.fandom.fandomnews.features.topwikis.presentation.grid.MostPopularWikisGridViewModel
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import kotlin.reflect.KClass

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MostPopularWikisListViewModel::class)
    abstract fun bindsListViewModel(listViewModel: MostPopularWikisListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MostPopularWikisGridViewModel::class)
    abstract fun bindsPhotosViewModel(gridViewModel: MostPopularWikisGridViewModel): ViewModel

    @MapKey
    @Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
    internal annotation class ViewModelKey(val value: KClass<out ViewModel>)

}