package com.wikia.fandom.fandomnews.common.base

import android.arch.lifecycle.ViewModelProvider
import android.support.v4.app.Fragment
import com.wikia.fandom.fandomnews.common.App
import com.wikia.fandom.fandomnews.common.di.AppComponent
import javax.inject.Inject

abstract class BaseFragment: Fragment() {

    val appComponent: AppComponent by lazy(mode = LazyThreadSafetyMode.NONE) {
        (activity?.application as App).appComponent
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
}