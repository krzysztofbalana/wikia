package com.wikia.fandom.fandomnews.features.topwikis.infrastructure

import com.google.gson.annotations.SerializedName

class MostPopularWikiaPhotosDTO(
        @SerializedName("image") val image: String
) {
}