package com.wikia.fandom.fandomnews.common

import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import kotlin.coroutines.experimental.CoroutineContext

abstract class UseCase<T>(
        private val uiContext: CoroutineContext,
        private val ioContext: CoroutineContext
) {

    abstract suspend fun buildUseCase(): T

    internal open fun execute(callback: (T) -> Unit) {
        launch(uiContext)  {
            try {
                val task = async(ioContext) { buildUseCase() }

                val result = task.await()
                callback.invoke(result)

            } catch (e:Exception) {
                e.printStackTrace()
            }

        }

    }
}