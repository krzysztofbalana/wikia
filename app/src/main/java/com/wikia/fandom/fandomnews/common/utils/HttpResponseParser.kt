package com.wikia.fandom.fandomnews.common.utils

import javax.net.ssl.HttpsURLConnection

object HttpResponseParser {

    fun parse(errorCode: Int): ErrorType {
        return when(errorCode) {
            HttpsURLConnection.HTTP_NOT_FOUND -> ErrorType.NOT_FOUND
            HttpsURLConnection.HTTP_BAD_REQUEST -> ErrorType.BAD_REQUEST
            else -> ErrorType.UNDEFINED
        }
    }
}


enum class ErrorType {
    UNDEFINED(), BAD_REQUEST(), NOT_FOUND()
}