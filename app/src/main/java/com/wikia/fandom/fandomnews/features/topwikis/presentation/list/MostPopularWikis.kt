package com.wikia.fandom.fandomnews.features.topwikis.presentation.list

sealed class MostPopularWikis {
    class Data(val topWikis: List<Wikia>): MostPopularWikis()
    class Error(val errorMessage: String): MostPopularWikis()
    class Wikia(val id: Int, val name: String, val articlesCount: Int)
}