package com.wikia.fandom.fandomnews.features.topwikis.domain

import com.wikia.fandom.fandomnews.common.utils.Mapper
import com.wikia.fandom.fandomnews.features.topwikis.presentation.list.MostPopularWikis
import com.wikia.fandom.fandomnews.features.topwikis.presentation.grid.MostPopularWikisPhotos


object WikiaEntityMapper : Mapper<List<MostPopularWikiaEntity>, List<MostPopularWikis.Wikia>> {

    override fun convert(data: List<MostPopularWikiaEntity>): List<MostPopularWikis.Wikia> {
        return data.map { MostPopularWikis.Wikia(it.id, it.name, it.articlesCount) }
    }

}

object WikiaPhotosEntityMapepr: Mapper<List<MostPopularWikiaPhotoEntity>, List<MostPopularWikisPhotos.WikiaPhoto>> {

    override fun convert(data: List<MostPopularWikiaPhotoEntity>): List<MostPopularWikisPhotos.WikiaPhoto> {
        return data.map { MostPopularWikisPhotos.WikiaPhoto(it.imageUrl) }
    }
}