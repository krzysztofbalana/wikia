package com.wikia.fandom.fandomnews.common.di

import com.wikia.fandom.fandomnews.common.App
import com.wikia.fandom.fandomnews.common.viewmodel.ViewModelModule
import com.wikia.fandom.fandomnews.features.topwikis.presentation.list.MostPopularWikisListFragment
import com.wikia.fandom.fandomnews.features.topwikis.MostPopularWikisModule
import com.wikia.fandom.fandomnews.features.topwikis.presentation.grid.MostPopularWikisGridFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    NetworkModule::class,
    ViewModelModule::class,
    MostPopularWikisModule::class])
interface AppComponent {

    fun inject(target: App)
    fun inject(target: MostPopularWikisListFragment)
    fun inject(target: MostPopularWikisGridFragment)
}