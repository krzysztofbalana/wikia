package com.wikia.fandom.fandomnews

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import butterknife.BindView
import butterknife.ButterKnife
import com.wikia.fandom.fandomnews.features.topwikis.presentation.grid.MostPopularWikisGridFragment
import com.wikia.fandom.fandomnews.features.topwikis.presentation.list.MostPopularWikisListFragment
import com.wikia.fandom.fandomnews.features.topwikis.MostPopularWikisPageAdapter

class HomeActivity : AppCompatActivity() {

    @BindView(R.id.tab_container)
    lateinit var viewPager: ViewPager
    @BindView(R.id.tabs)
    lateinit var tabsLayout: TabLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        ButterKnife.bind(this)
        val layoutViewPagerAdapter = MostPopularWikisPageAdapter(supportFragmentManager)
        layoutViewPagerAdapter
                .addFragment(MostPopularWikisListFragment(), "TITLE")
                .addFragment(MostPopularWikisGridFragment(), "GRID")
        viewPager.adapter = layoutViewPagerAdapter
        tabsLayout.setupWithViewPager(viewPager)

    }

}
