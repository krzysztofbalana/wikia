package com.wikia.fandom.fandomnews.common.networking

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class RestClient : RestApi {

    private val gson: Gson
    private val builder: Retrofit.Builder

    init {
        gson = GsonBuilder()
                .setLenient()
                .create()

        builder = Retrofit.Builder()
        builder.addConverterFactory(GsonConverterFactory.create(gson))
    }

    override fun load(): Retrofit {
        requireNotNull(builder.build().baseUrl(), { "Base url cannot be empty! Set one, before loading any query" })

        return builder.build()
    }

    open override fun using(url: String): RestApi {
        builder.baseUrl(url)
        return this
    }
}