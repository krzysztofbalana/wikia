package com.wikia.fandom.fandomnews.common

import com.wikia.fandom.fandomnews.common.utils.ErrorType

sealed class Result<out T>() {
    class Success<out T>(val data: T): Result<T>()
    class Failure<out T>(val typeOfError: ErrorType): Result<T>()
}