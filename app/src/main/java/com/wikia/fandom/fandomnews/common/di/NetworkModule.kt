package com.wikia.fandom.fandomnews.common.di

import com.wikia.fandom.fandomnews.common.networking.RestApi
import com.wikia.fandom.fandomnews.common.networking.RestClient
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class NetworkModule {

    @Singleton
    @Provides
    fun provideRestApiClient():RestApi = RestClient()

}
